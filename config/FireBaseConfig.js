import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import {
  getAuth,
  onAuthStateChanged,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  GoogleAuthProvider,
} from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyAgkBnB88sPQiObwukdNAxVpouPcz2zZYU",
  authDomain: "auth-react-4f8dc.firebaseapp.com",
  projectId: "auth-react-4f8dc",
  storageBucket: "auth-react-4f8dc.appspot.com",
  messagingSenderId: "29811469403",
  appId: "1:29811469403:web:fbbced3e919437d761a37e",
  measurementId: "G-1JEX65NW6H",
};
// DC:9F:CF:40:DB:B2:2F:A9:3A:C2:42:16:CD:05:9A:81:A2:B1:32:C8
const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);
const auth = getAuth(app);
// const db = getFirestore(app);

const registerWithEmailPasssword = async (name, email, password) => {
  createUserWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      // Signed in
      const user = userCredential.user;
      if (user) {
        return user;
      }
      // ...
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
      // ..
      console.log(errorMessage);
    });
};

const loginwithEmailPassword = async (email, password) => {
  try {
    const user = await (
      await signInWithEmailAndPassword(auth, email, password)
    ).user;
    // .then((userCredential) => {
    //   // Signed in
    //   const user = userCredential.user;
    //   console.log(user);
    //   // ...
    // })
    return user;
  } catch (error) {
    const errorCode = error.code;
    const errorMessage = error.message;
  }
};

const OAuth = async (user) => {
  try {
    let userId = "";
    await onAuthStateChanged(auth, (user) => {
      if (user) {
        const uid = user.uid;
        userId = uid;
        // console.log(uid);
        // ...
      } else {
        userId = null;
        // User is signed out
        // ...
      }
    });
    return userId;
  } catch (error) {
    // const errorCode = error.code;
    // const errorMessage = error.message;
    console.log(error);
  }
};
const signInWithGoogle = () => {
  try {
    signInWithPopup(auth, provider)
      .then((result) => {
        // This gives you a Google Access Token. You can use it to access the Google API.
        const credential = GoogleAuthProvider.credentialFromResult(result);
        const token = credential.accessToken;
        // The signed-in user info.
        const user = result.user;
        // IdP data available using getAdditionalUserInfo(result)
        // ...
      })
      .catch((error) => {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;
        // The email of the user's account used.
        const email = error.customData.email;
        // The AuthCredential type that was used.
        const credential = GoogleAuthProvider.credentialFromError(error);
        // ...
      });
  } catch (error) {
    console.log(error);
  }
};

export {
  registerWithEmailPasssword,
  loginwithEmailPassword,
  OAuth,
  signInWithEmailAndPassword,
  signInWithGoogle,
};
