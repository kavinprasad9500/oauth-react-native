import { Button, Image, StyleSheet, Text, View } from "react-native";
import React, { useEffect, useState } from "react";
import { OAuth } from "../config/FireBaseConfig";
import { useRoute } from "@react-navigation/native";
import { GoogleSignin } from '@react-native-google-signin/google-signin';

const Home = ({ route, navigation }) => {
  const { user } = route.params;
  const { userInfo } = route.params; //Google Auth
  const [text, setText] = useState("");

  useEffect(() => {
    if(user)
    auth();
    else
      setText("");

  });

  const auth = async () => {
    const auth = await OAuth(user);
    setText("OAuth : " + auth);
  };

  signOut = async () => {
    try {
      await GoogleSignin.signOut();
      setText("");
      navigation.navigate('Login');
      // Remember to remove the user from your app's state as well
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <View style={styles.container}>
      {text ? <Text style={{ fontSize: 20 }}>{text}</Text> : null}
      {userInfo?(
        <View style={{margin: 20}}>
          <Image style={{width: 100, height: 100, borderRadius: 50}} source={{uri:`${userInfo.user.photo}`}}/>
          <Text>Email : {userInfo.user.email}</Text>
          <Text>Name : {userInfo.user.name}</Text>
          <Text>Id : {userInfo.user.id}</Text>
        </View>
      ) : null}
      <Button title="Sign Out" onPress={signOut}/>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },
});

export default Home;
