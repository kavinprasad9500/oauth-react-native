import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  Dimensions,
  Image,
  ScrollView,
  Alert,
} from "react-native";
import React, { useEffect, useState } from "react";
import { Button, TextInput, Divider, IconButton } from "react-native-paper";
import {loginwithEmailPassword} from "../config/FireBaseConfig";
import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';

const screenWidth = Dimensions.get("window").width;

const Login = ({ navigation }) =>{
  GoogleSignin.configure({
    webClientId: '29811469403-rtf1fgoh34j4q193204q1dh1bpk1e2uq.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
  });

  signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      // setState({ userInfo });
      // console.log(userInfo);
      navigation.navigate('Home',{userInfo});
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
    }
  };

  const [showPassword, setShowPassword] = useState(false); // Add this state hook
  const [username, setUsername] = useState("kavinprasad333@gmail.com");
  const [password, setPassword] = useState("f9iiig2zzo");
  const [errorMsg, setErrorMsg] = useState("");
  const [userInfo, setUserInfo] = useState(null);

  const Login = async () =>{
    const user = await loginwithEmailPassword(username, password);
    // todo : Error Must Handle
    if(user){
      // console.log(user);
      navigation.navigate('Home',{user});
    }
  }


  // useEffect(() => {
  //   GoogleSignin.configure({
  //     webClientId: '29811469403-n3p2stai5c8kp4j9tpaddhrvfcqi2nmu.apps.googleusercontent.com',
  //   });
  // }, []);
  


  const GoogleLogin = () =>{
    // signIn = async () => {
    //   try {
    //     await GoogleSignin.hasPlayServices();
    //     const useInfo = await GoogleSignin.signIn();
    //     setUserInfo({ useInfo });
    //   } catch (error) {
    //     if (error.code === statusCodes.SIGN_IN_CANCELLED) {
    //       // user cancelled the login flow
    //     } else if (error.code === statusCodes.IN_PROGRESS) {
    //       // operation (e.g. sign in) is in progress already
    //     } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
    //       // play services not available or outdated
    //     } else {
    //       // some other error happened
    //     }
    //   }
    // };

  }
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#fff" barStyle="dark-content" />
      <ScrollView>
        <Text
          style={{
            marginVertical: 10,
            fontSize: 30,
            fontWeight: 500,
            textAlign: "center",
          }}
        >
          Login in to Hub
        </Text>

        <TextInput
          mode="outlined"
          placeholder="Username or Email"
          left={<TextInput.Icon icon="account" />}
          style={{ marginVertical: 5 }}
          theme={{ colors: { primary: "#15A801" } }}
          value={username}
          onChangeText={(username) => setUsername(username)}
        />

        <TextInput
          mode="outlined"
          placeholder="Password"
          secureTextEntry={!showPassword} // Toggle the secureTextEntry based on the state
          left={<TextInput.Icon icon="lock" />}
          right={
            <TextInput.Icon
              icon={showPassword ? "eye-off" : "eye"}
              onPress={() => setShowPassword(!showPassword)}
            />
          } // Toggle the icon and state on button press
          style={{ marginVertical: 5 }}
          theme={{ colors: { primary: "#15A801" } }}
          value={password}
          onChangeText={(password) => setPassword(password)}
        />

        {errorMsg ? (
          <Text
            style={{
              marginVertical: 5,
              fontWeight: 600,
              textAlign: "center",
              color: "#ff0000",
            }}
          >
            {errorMsg}
          </Text>
        ) : null}

        <Button
          mode="contained"
          style={{ marginVertical: 15 }}
          buttonColor="#15A801"
          onPress={Login}
        >
          Log In
        </Button>

        <Button
          mode="contained"
          onPress={() => navigation.navigate("SignUp")}
          style={{ marginHorizontal: "25%", marginBottom: 10 }}
          buttonColor="#fff"
          textColor="#0074b7"
        >
          Sign Up
        </Button>

        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Divider style={{ flex: 1, marginHorizontal: 10 }} />
          <Text>or</Text>
          <Divider style={{ flex: 1, marginHorizontal: 10 }} />
        </View>

        {/* <Button mode="contained" style={{ marginVertical: 15 }} buttonColor="#4286F5">Log In</Button> */}

        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            marginTop: 20,
          }}
        >
          <IconButton
            icon={() => (
              <Image
                source={{
                  uri: "https://www.freepnglogos.com/uploads/google-logo-png/google-logo-png-webinar-optimizing-for-success-google-business-webinar-13.png",
                }}
                style={{ width: 24, height: 24 }}
              />
            )}
            style={{
              alignSelf: "center",
              borderColor: "#4286F5",
              borderWidth: 1,
            }}
            onPress={signIn}
          >
            Login with Google
          </IconButton>
          <IconButton
            icon={() => (
              <Image
                source={{
                  uri: "https://www.freepnglogos.com/uploads/apple-logo-png/apple-logo-png-dallas-shootings-don-add-are-speech-zones-used-4.png",
                }}
                style={{ width: 24, height: 24 }}
              />
            )}
            style={{ alignSelf: "center", borderColor: "#000", borderWidth: 1 }}
            onPress={() => console.log("Pressed")}
          >
            Login with Google
          </IconButton>
        </View>
      </ScrollView>
    </View>
  );
}
export default Login;3
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 15,
    justifyContent: "center",
  },
});
