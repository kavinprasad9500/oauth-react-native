import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  Dimensions,
  Image,
  ScrollView,
} from "react-native";
import React, { useEffect, useState } from "react";
import { Button, TextInput, Divider, Checkbox } from "react-native-paper";
import {registerWithEmailPasssword} from "../config/FireBaseConfig";
import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';



const screenWidth = Dimensions.get("window").width;

export default function SignUp({ navigation }) {
  GoogleSignin.configure({
    webClientId: '29811469403-rtf1fgoh34j4q193204q1dh1bpk1e2uq.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
  });

  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setConfirmShowPassword] = useState(false);
  const [button, setButton] = useState(true);

  signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      // setState({ userInfo });
      // console.log(userInfo);
      navigation.navigate('Home',{userInfo});
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
    }
  };

  const [errorMsg, setErrorMsg] = useState("");
  useEffect(() => {
    checkConfirm();
  }, [confirmPassword]);

  const checkConfirm = () => {
    if (password === confirmPassword) {
      setErrorMsg("");
      setButton(false);
    } else {
      setErrorMsg("Password Doesn't Match");
      setButton(true);
    }
  };

  const SignUp = () => {
    // Todo : Validation is must and Error Must be Handling
    const register = registerWithEmailPasssword(username, email, password);
    if(register)
      navigation.navigate('Home');
  };

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#fff" barStyle="dark-content" />

      <Text
        style={{
          marginVertical: 10,
          fontSize: 30,
          fontWeight: 500,
          textAlign: "center",
        }}
      >
        Sign up
      </Text>

      <Button
        icon="google"
        mode="contained"
        style={{ marginVertical: 15 }}
        buttonColor="#4286F5"
        onPress={signIn}
      >
        Continue with Google
      </Button>

      <View style={{ flexDirection: "row", alignItems: "center" }}>
        <Divider style={{ flex: 1, marginHorizontal: 10 }} />
        <Text>or</Text>
        <Divider style={{ flex: 1, marginHorizontal: 10 }} />
      </View>

      <TextInput
        mode="outlined"
        placeholder="Username"
        style={{ marginVertical: 5 }}
        theme={{ colors: { primary: "#15A801" } }}
        onChangeText={(username) => setUsername(username)}
        value={username}
      />

      <TextInput
        mode="outlined"
        placeholder="Email"
        style={{ marginVertical: 5 }}
        theme={{ colors: { primary: "#15A801" } }}
        onChangeText={(email) => setEmail(email)}
        value={email}
      />

      <TextInput
        mode="outlined"
        placeholder="Password"
        secureTextEntry={!showPassword}
        right={
          <TextInput.Icon
            icon={showPassword ? "eye-off" : "eye"}
            onPress={() => setShowPassword(!showPassword)}
          />
        }
        style={{ marginVertical: 5 }}
        theme={{ colors: { primary: "#15A801" } }}
        onChangeText={(password) => setPassword(password)}
        value={password}
      />

      <TextInput
        mode="outlined"
        placeholder="Confirm password"
        secureTextEntry={!showConfirmPassword}
        right={
          <TextInput.Icon
            icon={showConfirmPassword ? "eye-off" : "eye"}
            onPress={() => setConfirmShowPassword(!showConfirmPassword)}
          />
        }
        style={{ marginVertical: 5 }}
        theme={{ colors: { primary: "#15A801" } }}
        onChangeText={(confirmPass) => setConfirmPassword(confirmPass)}
        value={confirmPassword}
      />

      {errorMsg ? (
        <Text
          style={{
            marginVertical: 5,
            fontWeight: 600,
            textAlign: "center",
            color: "#ff0000",
          }}
        >
          {errorMsg}
        </Text>
      ) : null}

      <Button
        mode="contained"
        style={{ marginVertical: 15 }}
        buttonColor="#15A801"
        onPress={SignUp}
        disabled={button}
      >
        Create my account
      </Button>

      <Button
        mode="contained"
        onPress={() => navigation.navigate("Login")}
        style={{ marginBottom: 10 }}
        buttonColor="#fff"
        textColor="#0074b7"
      >
        Already have an account? Login
      </Button>

      {/* <Button mode="contained" style={{ marginVertical: 15 }} buttonColor="#4286F5">Log In</Button> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 15,
  },
  dropdown1BtnStyle: {
    width: "100%",
    height: 50,
    backgroundColor: "#FFF",
    borderRadius: 8,
    borderWidth: 1,
    borderColor: "#777",
    marginVertical: 10,
  },
  dropdown1BtnTxtStyle: {
    color: "#777",
    textAlign: "left",
    fontSize: 14,
  },
  dropdown1DropdownStyle: {
    backgroundColor: "#EFEFEF",
  },
  dropdown1RowStyle: {
    backgroundColor: "#EFEFEF",
    borderBottomColor: "#C5C5C5",
  },
  dropdown1RowTxtStyle: {
    color: "#444",
    textAlign: "left",
  },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 20,
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    padding: 5,
    paddingRight: 25,
    lineHeight: 15,
  },
  inner: {
    color: "#15A801",
  },
});
